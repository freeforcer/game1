﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    sealed class Map
    {
        private readonly int width;
        private readonly int height;
        private readonly int mapSize;
        private const int step = 55;
        private readonly LandScape[,] landScape;
        private Point screenTarget;
        private List<Unit> units;
        private List<Unit> selectedUnits;
        private List<Building> buildings;
        private Building selectedBuildings;
        private readonly List<Bitmap> sprites;
        private readonly List<Bitmap> spritesMiniMap;
        private readonly Random rand;
        private readonly Pen pen;
        private Point[] miniMapCoordinates;
        private struct LandScape
        {
            public int landScape;
            public bool isBlocked;
        }

        public Map(int mapSize, int width, int height)
        {
            this.width = width;
            this.height = height;
            this.mapSize = mapSize;
            landScape = new LandScape[mapSize, mapSize];
            units = new List<Unit>();
            selectedUnits = new List<Unit>();
            buildings = new List<Building>();
            rand = new Random();
            pen = new Pen(Color.White, 1);
            for (int i = 0; i < mapSize; i++)
            {
                for (int j = 0; j < mapSize; j++)
                {
                    landScape[i, j].landScape = rand.Next(0, 2);
                    if (landScape[i, j].landScape == 1) landScape[i, j].isBlocked = true;
                }
            }
            sprites = new List<Bitmap>()
            {
                new Bitmap(@"Grass.jpg"),
                new Bitmap(@"Water.jpg"),
            };
            spritesMiniMap = new List<Bitmap>()
            {
                new Bitmap(@"Grass_mini.jpg"),
                new Bitmap(@"Water_mini.jpg"),
            };
            miniMapCoordinates = new Point[4];
            /*{
                new Point(width + step * 2, 0),
                new Point(width + step * 2 + width / step, 0),
                new Point(width + step * 2 + width / step, height / step),
                new Point(width + step * 2, height / step),
            };*/
        }
        public void CreateUnit()
        {
            if (selectedBuildings == null) return;

            Point position = new Point();
            position = selectedBuildings.CreateUnit();

            units.Add(new Unit(position));
            foreach (Unit u in units)
            {
                landScape[u.Position.X / step, u.Position.Y / step].isBlocked = true;
            }
        }
        public void CreateBuilding(Point position)
        {
            if (position.X > width + step - (step / 5) ||
                position.Y > height + step - (step / 5)) return;
            Round(ref position);

            position.X += screenTarget.X * step;
            position.Y += screenTarget.Y * step;

            buildings.Add(new Building(position));
            foreach (Building b in buildings)
            {
                for (int i = 0; i < b.Position.Length; i++)
                {
                    landScape[b.Position[i].X / step, b.Position[i].Y / step].isBlocked = true;
                }
            }
        }
        public void SelectObject(Point position)
        {
            if (position.X > width + step - (step / 5) ||
                position.Y > height + step - (step / 5)) return;
            Round(ref position);

            selectedUnits.Clear();
            selectedBuildings = null;

            position.X += screenTarget.X * step;
            position.Y += screenTarget.Y * step;

            foreach(Unit u in units)
            {
                if (position == u.Position)
                {
                    selectedUnits.Add(u);
                    break;
                }
            }

            foreach(Building b in buildings)
            {
                if (position == b.Position[0])
                {
                    selectedBuildings = b;
                    break;
                }
            }
        }
        public void MoveUnit(Point destination)
        {
            if (destination.X > width + step - (step / 5) || 
                destination.Y > height + step - (step / 5)) return;
            Round(ref destination);

            destination.X += screenTarget.X * step;
            destination.Y += screenTarget.Y * step;

            Point temp;
            foreach (Unit u in selectedUnits)
            {
                temp = u.Position;
                u.Move(destination);
                landScape[temp.X / step, temp.Y / step].isBlocked = false;
                landScape[u.Position.X / step, u.Position.Y / step].isBlocked = true;
            }
        }
        private void Round(ref Point position)
        {
            position.X /= step;
            position.X *= step;

            position.Y /= step;
            position.Y *= step;
        }
        public void MoveScreenUp() { if (screenTarget.Y > 0) screenTarget.Y--; }
        public void MoveScreenDown() { if (screenTarget.Y + height / step + 1 < mapSize) screenTarget.Y++; }
        public void MoveScreenLeft() { if (screenTarget.X > 0) screenTarget.X--; }
        public void MoverScreenRight() { if (screenTarget.X + width / step + 1 < mapSize) screenTarget.X++; }
        public void Draw(PaintEventArgs e)
        {
            //drawing map
            for (int i = 0; i < width; i += step)
            {
                for (int j = 0; j < height; j += step)
                {
                    e.Graphics.DrawImage(sprites[landScape[i / step + screenTarget.X, j / step + screenTarget.Y].landScape], i, j);
                }
            }

            //drawing minimap
            for (int i = 0; i < mapSize * 2; i += 2)
            {
                for (int j = 0; j < mapSize * 2; j += 2)
                {
                    e.Graphics.DrawImage(spritesMiniMap[landScape[i / 2, j / 2].landScape], 
                                         width + step * 2 + i, j);
                }
            }
            miniMapCoordinates[0].X = width + step * 2 + screenTarget.X * 2;
            miniMapCoordinates[0].Y = screenTarget.Y * 2;
            miniMapCoordinates[1].X = miniMapCoordinates[0].X + width / step * 2;
            miniMapCoordinates[1].Y = miniMapCoordinates[0].Y;
            miniMapCoordinates[2].X = miniMapCoordinates[0].X + width / step * 2;
            miniMapCoordinates[2].Y = miniMapCoordinates[0].Y + height / step * 2;
            miniMapCoordinates[3].X = miniMapCoordinates[0].X;
            miniMapCoordinates[3].Y = miniMapCoordinates[0].Y + height / step * 2;
            e.Graphics.DrawPolygon(pen, miniMapCoordinates);

            //drawing units and buildings
            if (units != null)
                foreach (var u in units)
                    u.Draw(e, screenTarget, false, width, height);

            if (selectedUnits != null)
                foreach (var u in selectedUnits)
                    u.Draw(e, screenTarget, true, width, height);

            if (buildings != null)
                foreach (var b in buildings)
                    b.Draw(e, screenTarget, false, width, height);

            if (selectedBuildings != null)
                selectedBuildings.Draw(e, screenTarget, true, width, height);
        }
    }
}
