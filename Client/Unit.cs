﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Client
{
    class Unit
    {
        private const int step = 55;
        private Point position;
        private Point[] coordinates;
        private Pen pen;

        public Unit(Point position)
        {
            this.position = position;
            pen = new Pen(Color.Blue, 2);
            coordinates = new Point[3];
            {
                new Point(position.X + step / 2 + 1, position.Y);
                new Point(position.X + step, position.Y + step);
                new Point(position.X, position.Y + step);
            }
        }
        public Point Position
        {
            get { return position; }
        }
        public void Move(Point destination)
        {
            if (position.X > destination.X) position.X -= step;
            if (position.X < destination.X) position.X += step;
            if (position.Y > destination.Y) position.Y -= step;
            if (position.Y < destination.Y) position.Y += step;
        }
        public void Draw(PaintEventArgs e, Point screenTarget, bool isSelected, int width, int height)
        {
            if (isSelected)
                pen.Color = Color.Red;
            else
                pen.Color = Color.Blue;

            coordinates[0].X = position.X - screenTarget.X * step + (step / 2 + 1);
            coordinates[0].Y = position.Y - screenTarget.Y * step;
            coordinates[1].X = position.X - screenTarget.X * step + step;
            coordinates[1].Y = position.Y - screenTarget.Y * step + step;
            coordinates[2].X = position.X - screenTarget.X * step;
            coordinates[2].Y = position.Y - screenTarget.Y * step + step;

            if (position.X < screenTarget.X * step + width && position.Y < screenTarget.Y * step + height)
                e.Graphics.DrawPolygon(pen, coordinates);
        }
    }
}
