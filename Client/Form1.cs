﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        Map map;

        public Form1()
        {
            InitializeComponent();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            map = new Map(100, 935, 660);
            this.Invalidate();
        }
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                map.MoveUnit(e.Location);
                Invalidate();
            }
            if (e.Button == MouseButtons.Left)
            {
                map.SelectObject(e.Location);
                Invalidate();
            }
            if (e.Button == MouseButtons.Middle)
            {
                map.CreateBuilding(e.Location);
                Invalidate();
            }
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up) map.MoveScreenUp();
            if (e.KeyCode == Keys.Down) map.MoveScreenDown();
            if (e.KeyCode == Keys.Left) map.MoveScreenLeft();
            if (e.KeyCode == Keys.Right) map.MoverScreenRight();
            if (e.KeyCode == Keys.A) map.CreateUnit();
            Invalidate();
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            map.Draw(e);
        }
    }
}
