﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client
{
    class Building
    {
        private const int step = 55;
        private const int buildingSize = 2;
        private Point[] position;
        private Point[] coordinates;
        private Pen pen;

        public Building(Point position)
        {
            this.position = new Point[buildingSize * 2]
            {
                new Point(position.X, position.Y),
                new Point(position.X + step, position.Y),
                new Point(position.X + step, position.Y + step),
                new Point(position.X, position.Y + step),
            };
            pen = new Pen(Color.Blue, 2);
            coordinates = new Point[4]
            {
                new Point(position.X, position.Y),
                new Point(position.X + step * 2, position.Y),
                new Point(position.X + step * 2, position.Y + step * 2),
                new Point(position.X, position.Y + step * 2),
            };
        }
        public Point[] Position
        {
            get { return position; }
        }
        public int BuildingSize
        {
            get { return buildingSize; }
        }
        public Point CreateUnit()
        {
            return new Point(position[0].X - step, position[0].Y - step);
        }
        public void Draw(PaintEventArgs e, Point screenTarget, bool isSelected, int width, int height)
        {
            if (isSelected)
                pen.Color = Color.Red;
            else
                pen.Color = Color.Blue;

            coordinates[0].X = position[0].X - screenTarget.X * step;
            coordinates[0].Y = position[0].Y - screenTarget.Y * step;
            coordinates[1].X = position[0].X - screenTarget.X * step + step * 2;
            coordinates[1].Y = position[0].Y - screenTarget.Y * step;
            coordinates[2].X = position[0].X - screenTarget.X * step + step * 2;
            coordinates[2].Y = position[0].Y - screenTarget.Y * step + step * 2;
            coordinates[3].X = position[0].X - screenTarget.X * step;
            coordinates[3].Y = position[0].Y - screenTarget.Y * step + step * 2;

            if (position[0].X < screenTarget.X * step + width && position[0].Y < screenTarget.Y * step + height)
                e.Graphics.DrawPolygon(pen, coordinates);
        }
    }
}
